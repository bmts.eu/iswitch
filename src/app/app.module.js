(function() {
  'use strict';

  angular.module('iSwitch', [
    'ngMaterial',
    'ngMessages',
    'pascalprecht.translate',
    'tmh.dynamicLocale',
    'ui.router',
  ]);

})();

(function () {
    'use strict';

    angular.module('iSwitch').config(configBlock);

    /** @ngInject */
    function configBlock($mdThemingProvider, $locationProvider, $logProvider, $translateProvider,
                         tmhDynamicLocaleProvider) {
        $mdThemingProvider.theme('default')
            .dark();
        $locationProvider.html5Mode(true);

        $logProvider.debugEnabled(true);

        $translateProvider.useStaticFilesLoader(
            {
                prefix: '/locales/',
                suffix: '.json'
            }
        )
            .preferredLanguage('en')
            .fallbackLanguage('en')
            .useSanitizeValueStrategy('escape')
            .useMissingTranslationHandlerLog();

        tmhDynamicLocaleProvider
            .localeLocationPattern('/locales/angular-locale_{{locale}}.js')
            .defaultLocale('en');
    }

})();

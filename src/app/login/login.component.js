(function() {
    'use strict';
    angular.module('iSwitch').component('login', {
        controller: LoginController,
        controllerAs: 'vm',
        templateUrl: 'app/login/login.view.html',
    });
    /** @ngInject */
    function LoginController($log, $rootScope, $translate) {
        const vm = this;

        vm.switchLanguage = switchLanguage;

        activate();

        function activate() {
            $log.debug('login activated');
        }

        function switchLanguage(language) {
            $translate.use(language);
        }
    }
})();

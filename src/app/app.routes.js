(function () {
    'use strict';

    angular.module('iSwitch').config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                component: 'home'
            })
            .state('login', {
                url: '/login',
                component: 'login'
            });

        $urlRouterProvider.otherwise('/');
    }

})();
